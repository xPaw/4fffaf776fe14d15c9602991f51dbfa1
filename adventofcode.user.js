// ==UserScript==
// @name         Advent of Code Improvements
// @namespace    https://gist.github.com/xPaw/4fffaf776fe14d15c9602991f51dbfa1
// @updateURL    https://gist.github.com/xPaw/4fffaf776fe14d15c9602991f51dbfa1
// @version      1.2
// @author       xPaw
// @match        https://adventofcode.com/*
// @icon         https://adventofcode.com/favicon.png
// @grant        GM_addStyle
// @run-at       document-end
// ==/UserScript==

(function() {
	'use strict';

	GM_addStyle(`
		body {
			font-family: 'JetBrains Mono', ui-monospace, monospace;
			font-weight: 400;
			font-size: 18px;
			max-width: 60em;
			min-width: unset;
			margin: 20px auto;
			padding: 0 20px;
		}

		p.wrap,
		article {
			width: unset;
			max-width: 45em;
		}

		header,
		article h2 {
			white-space: unset;
		}

		pre, code {
			font-family: inherit;
			font-variant-ligatures: none;
		}

		#sidebar {
			display: none;
		}

		code:before {
			border: 1px solid #666;
			background: #000;
		}

		pre > code:before {
			left: -6px;
			right: -6px;
		 	top: -6px;
			bottom: -6px;
		}
	`);

	const viewport = document.createElement('meta');
	viewport.name = 'viewport';
	viewport.content = 'width=device-width, initial-scale=1.0';
	document.head.append(viewport);
})();
